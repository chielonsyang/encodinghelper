package edu.carleton.yangch;

import java.nio.charset.Charset;
import java.util.Scanner;
import java.io.UnsupportedEncodingException;
import java.io.File;
import java.io.FileNotFoundException;

/**
 * EncodingHelperChar
 *
 * The main support class for the EncodingHelper project in
 * CS 257, Fall 2014, Carleton College. Each object of this class
 * represents a single Unicode character. The class's methods
 * generate various representations of the character. 
 */

public class EncodingHelperChar {
    private int codePoint;
    //private byte[] utf8Bytes;
    //private char ch;

    public EncodingHelperChar(int codePoint) {
        this.codePoint = codePoint;
    }

    public EncodingHelperChar(byte[] utf8Bytes) {
        //this.utf8Bytes = utf8Bytes;
        String s = new String(utf8Bytes, Charset.forName("UTF-8"));
        //System.out.println("THE CONSTRUCT");
        //System.out.println(s);
        this.codePoint = s.codePointAt(0);
    }

    public EncodingHelperChar(char ch) {
        //this.ch = ch;
        String s = Character.toString(ch);
        this.codePoint = s.codePointAt(0);
        //this.codePoint = Integer.parseInt(s);
    }

    public int getCodePoint() {
        return this.codePoint;
    }

    public void setCodePoint(int codePoint) {
        this.codePoint = codePoint;
    }

    /**
     * Converts this character into an array of the bytes in its UTF-8 representation.
     * For example, if this character is a lower-case letter e with an acute accent, 
     * whose UTF-8 form consists of the two-byte sequence C3 A9, then this method returns
     * a byte[] of length 2 containing C3 and A9.
     *
     * @return the UTF-8 byte array for this character
     */
    public byte[] toUTF8Bytes() {
        //String s = Character.toString(this.ch);
        String s = Character.toString((char)this.codePoint);
        //System.out.println("This string:");
        //System.out.println(s);
        //System.out.println("This cp:");
        //System.out.println(this.codePoint);
        byte[] utf8Array = new byte[] {};
        try {
            utf8Array = s.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            System.err.println(e);
            System.exit(1);
        }

        return utf8Array;
    }

    /**
     * Generates the conventional 4-digit hexadecimal code point notation for this character.
     * For example, if this character is a lower-case letter e with an acute accent,
     * then this method returns the string U+00E9 (no quotation marks in the returned String).
     *
     * @return the U+ string for this character
     */
    public String toCodePointString() {
        String answer;
        answer = String.format("U+%04x", this.codePoint);
        answer.toUpperCase();
        //System.out.println(answer);
        return answer;
    }

    /**
     * Generates a hexadecimal representation of this character suitable for pasting into a
     * string literal in languages that support hexadecimal byte escape sequences in string
     * literals (e.g. C, C++, and Python). For example, if this character is a lower-case
     * letter e with an acute accent (U+00E9), then this method returns the
     * string \xC3\xA9. Note that quotation marks should not be included in the returned String.
     *
     * @return the escaped hexadecimal byte string
     */
    public String toUTF8StringWithoutQuotes() {
        //String(int[] codePoints, int offset, int count)
        //new String(bytes, 1, 2)
        String s = Character.toString((char)this.codePoint);
        //String s = Character.toString(this.ch);
        String fullString = "";
        String answer = "";
        try {
            //System.out.println("\n=== Bytes in command-line argument, considered as UTF-8 ===");
            byte[] bytes = s.getBytes("UTF-8");
            for (byte b : bytes) {
                answer = String.format("\\x%02x", ((int)b) & 0x000000ff);
                fullString = fullString + answer;
                //System.out.print(answer);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return answer;
    }

    /**
     * Generates the official Unicode name for this character.
     * For example, if this character is a lower-case letter e with an acute accent,
     * then this method returns the string "LATIN SMALL LETTER E WITH ACUTE" (without quotation marks).
     *
     * @return this character's Unicode name
     */
    public String getCharacterName() {
        String s = Character.getName(this.codePoint);
        return s;
    }
}

