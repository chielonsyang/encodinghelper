package edu.carleton.yangch;

import org.junit.Assert;
import org.junit.Test;

/**
 * EncodingHelperChar
 *
 * The main test class for the EncodingHelper project in
 * CS 257, Fall 2014, Carleton College. Each object the
 * EncodingHelperChar contains a single Unicode character.
 * The class's methods test the methods in EncodingHelperChar.
 */

public class EncodingHelperCharTest {

    @Test
    public void testGetCodePoint() throws Exception {
        System.out.println("Testing getCodePoint method: ");

        //Test codePoint constructor
        EncodingHelperChar cpToCP = new EncodingHelperChar(97);
        Assert.assertEquals(97, cpToCP.getCodePoint());

        //Test byte constructor
        String s = Character.toString((char) 97);
        System.out.println(s);
        byte[] utf8ch = s.getBytes("UTF-8");
        EncodingHelperChar byteToCP = new EncodingHelperChar(utf8ch);
        Assert.assertEquals(97, byteToCP.getCodePoint());

        //Test char constructor
        EncodingHelperChar chToCP = new EncodingHelperChar('a');
        Assert.assertEquals(97, chToCP.getCodePoint());
    }

    @Test
    public void testSetCodePoint() throws Exception {
        System.out.println("Testing setCodePoint: ");

        //Test codePoint constructor
        EncodingHelperChar cpToCP = new EncodingHelperChar(97);
        cpToCP.setCodePoint(61);
        Assert.assertEquals(61, cpToCP.getCodePoint());

        //Test byte constructor
        String s = Character.toString((char)97);
        //System.out.println(s);
        byte[] utf8ch = s.getBytes("UTF-8");
        EncodingHelperChar byteToCP = new EncodingHelperChar(utf8ch);
        byteToCP.setCodePoint(61);
        Assert.assertEquals(61, byteToCP.getCodePoint());

        //Test char constructor
        EncodingHelperChar chToCP = new EncodingHelperChar('a');
        chToCP.setCodePoint(61);
        Assert.assertEquals(61, chToCP.getCodePoint());
        String ss = Character.toString((char)chToCP.getCodePoint());
        System.out.println(ss);
    }

    @Test
    public void testToUTF8Bytes() throws Exception {
        System.out.println("testToUTF8Bytes:");
        EncodingHelperChar ch = new EncodingHelperChar('a');
        for(byte byt : ch.toUTF8Bytes()){
            //System.out.print("cp:");
            //System.out.print(byt);
            Assert.assertEquals(byt, 97);
        }
        EncodingHelperChar cp = new EncodingHelperChar(97);
        for(byte byt : cp.toUTF8Bytes()){
            //System.out.print("CP:");
            //System.out.print(byt);
            Assert.assertEquals(byt, 97);
        }


        //ch.setCodePoint(256);



    }

    @Test
    public void testToCodePointString() throws Exception {
        System.out.println("testing toCodePointString: ");
        EncodingHelperChar encodeCh = new EncodingHelperChar('a');
        Assert.assertEquals("U+0061", encodeCh.toCodePointString());

        EncodingHelperChar encodeInt = new EncodingHelperChar(97);
        Assert.assertEquals("U+0061", encodeInt.toCodePointString());

        String s = Character.toString((char)97);
        //System.out.println(s);
        byte[] utf8ch = s.getBytes("UTF-8");
        EncodingHelperChar encodeByte = new EncodingHelperChar(utf8ch);
        Assert.assertEquals("U+0061", encodeByte.toCodePointString());

        //System.out.println(encodeCh.toCodePointString());
        //System.out.println(encodeInt.toCodePointString());
        //System.out.println(encodeByte.toCodePointString());

    }

    @Test
    public void testToUTF8StringWithoutQuotes() throws Exception {
        System.out.println("testToUTF8StringWithoutQuotes:");

        EncodingHelperChar chTest = new EncodingHelperChar('*');
        System.out.println(chTest.toUTF8StringWithoutQuotes());
        System.out.println(chTest.getCodePoint());
        Assert.assertEquals("\\x2a", chTest.toUTF8StringWithoutQuotes());

        EncodingHelperChar intTest = new EncodingHelperChar(97);
        System.out.println(intTest.toUTF8StringWithoutQuotes());
        Assert.assertEquals("\\x61", intTest.toUTF8StringWithoutQuotes());

        String s = Character.toString((char)42);
        System.out.println(s);
        byte[] utf8ch = s.getBytes("UTF-8");
        EncodingHelperChar byteTest = new EncodingHelperChar(utf8ch);
        System.out.println(byteTest.toUTF8StringWithoutQuotes());
        Assert.assertEquals("\\x2a", byteTest.toUTF8StringWithoutQuotes());
    }


    @Test
    public void testGetCharacterName() throws Exception {
        System.out.println("testGetCharacterName:");

        EncodingHelperChar chTest = new EncodingHelperChar('a');
        chTest.getCharacterName();
        //System.out.println(chTest.getCodePoint());
        Assert.assertEquals("LATIN SMALL LETTER A", chTest.getCharacterName());

        EncodingHelperChar cpTest = new EncodingHelperChar(97);
        cpTest.getCharacterName();
        //System.out.println(cpTest.getCodePoint());
        Assert.assertEquals("LATIN SMALL LETTER A", cpTest.getCharacterName());

        String s = Character.toString((char)97);
        byte[] utf8ch = s.getBytes("UTF-8");
        EncodingHelperChar byteTest = new EncodingHelperChar(utf8ch);
        byteTest.getCharacterName();
        //System.out.println(chTest.getCodePoint());
        Assert.assertEquals("LATIN SMALL LETTER A", byteTest.getCharacterName());
    }

    public static void main(String[] args){

        EncodingHelperCharTest tester = new EncodingHelperCharTest();
        try {
            //tester.testGetCodePoint();
            //tester.testSetCodePoint();
            //tester.testToUTF8Bytes();
            //tester.testToCodePointString();
            //tester.testToUTF8StringWithoutQuotes();
            tester.testGetCharacterName();
        } catch (Exception e) {
            System.err.println(e);
            System.exit(1);
        }
    }
}
