package edu.carleton.yangch;

import java.util.ArrayList;

/**
 * Created by Camille and Chielon. 9/30/14
 * A program that takes command line arguments and converts
 * them based on the desired input and output.
 */
public class EncodingHelper {
    private String inputType = "string";
    private String outputType = "summary";
    private String data;
    private byte[] hexAsByte;
    private char dataChar;
    private int dataInt;

    public void HexToByteArray() {
        int len = this.data.length();
        this.hexAsByte = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            this.hexAsByte[i / 2] = (byte) ((Character.digit(this.data.charAt(i), 16) << 4)
                    + Character.digit(this.data.charAt(i+1), 16));
        }
    }

    /**
     * Prints out a summary for the given character.
     */
    public void PrintSingleCharacterSummary() {
        for (int k = 0; k < this.data.length(); k++) {
            char ch = this.data.charAt(k);
            EncodingHelperChar argument = new EncodingHelperChar(ch);
            System.out.println("Character: " + this.data);
            System.out.println("Code point: " + argument.toCodePointString());
            System.out.println("Name: " + argument.getCharacterName());
            System.out.println("UTF8: " + argument.toUTF8StringWithoutQuotes());
        }
    }

    /**
     * Prints out a summary for the given, multi-character string.
     */
    public void PrintStringSummary() {
        ArrayList<String> codePointList = new ArrayList<String>();
        ArrayList<String> nameList = new ArrayList<String>();
        ArrayList<String> utf8List = new ArrayList<String>();
        if (this.inputType.equals("string")) {
            System.out.println("String: " + this.data);
            for (int i = 0; i < this.data.length(); i++) {
                this.dataChar = this.data.charAt(i);
                EncodingHelperChar stringCharacter = new EncodingHelperChar(this.dataChar);
                codePointList.add(stringCharacter.toCodePointString());
                nameList.add(stringCharacter.getCharacterName());
                utf8List.add(stringCharacter.toUTF8StringWithoutQuotes());
            }
        }
        if (this.inputType.equals("utf8")) {
            EncodingHelperChar stringCharacter = new EncodingHelperChar(this.hexAsByte);
            codePointList.add(stringCharacter.toCodePointString());
            nameList.add(stringCharacter.getCharacterName());
            utf8List.add(stringCharacter.toUTF8StringWithoutQuotes());
            System.out.print("String: ");
            System.out.println(Character.toChars(stringCharacter.getCodePoint()));
            System.out.println("Name: " + stringCharacter.getCharacterName());
        }
        if (this.inputType.equals("codepoint")) {
            this.dataInt = this.data.codePointAt(0);
            EncodingHelperChar stringCharacter = new EncodingHelperChar(this.dataInt);
            System.out.print("String: ");
            System.out.println(Character.toChars(stringCharacter.getCodePoint()));
            codePointList.add(stringCharacter.toCodePointString());
            nameList.add(stringCharacter.getCharacterName());
            utf8List.add(stringCharacter.toUTF8StringWithoutQuotes());
            System.out.println("Name: " + stringCharacter.getCharacterName());
        }
        System.out.print("Code points: ");
        for (int j = 0; j < codePointList.size(); j++ ){
            System.out.print(codePointList.get(j) + " ");
        }
        System.out.println();
        System.out.print("UTF8: ");
        for (int k = 0; k < utf8List.size(); k++ ){
            System.out.print(utf8List.get(k));
        }
        System.out.println();
    }

    public void PrintWithInputOutput() {
        if (this.inputType.equals("string")) {
            if (this.outputType.equals("summary")) {
                PrintStringSummary();
            } else if (this.outputType.equals("utf8")) {
                for (int i = 0; i < this.data.length(); i++) {
                    char c = this.data.charAt(i);
                    EncodingHelperChar ehc = new EncodingHelperChar(c);
                    System.out.println(ehc.toUTF8StringWithoutQuotes());
                }
            } else if (this.outputType.equals("codepoint")) {
                for (int i = 0; i < this.data.length(); i++) {
                    char c = this.data.charAt(i);
                    EncodingHelperChar ehc = new EncodingHelperChar(c);
                    System.out.println(ehc.toCodePointString());
                }
            }
        } else if (this.inputType.equals("utf8")) {
            this.data = this.data.replace("\\x", "");
            HexToByteArray();
            if (this.outputType.equals("summary")) {
                PrintStringSummary();
            } else if (this.outputType.equals("string")) {
                EncodingHelperChar stringCharacter = new EncodingHelperChar(this.hexAsByte);
                System.out.print("String: ");
                System.out.println(Character.toChars(stringCharacter.getCodePoint()));
                System.out.println("Name: " + stringCharacter.getCharacterName());
            } else if (this.outputType.equals("codepoint")) {
                ArrayList<String> codePointList = new ArrayList<String>();
                EncodingHelperChar stringCharacter = new EncodingHelperChar(this.hexAsByte);
                codePointList.add(stringCharacter.toCodePointString());
                System.out.print("Code points: ");
                for (int j = 0; j < codePointList.size(); j++ ){
                    System.out.print(codePointList.get(j) + " ");
                }
                System.out.println();
            }
        } else if (this.inputType.equals("codepoint")) {
            if (this.outputType.equals("summary")) {
                String utf8String = "";
                for (int i = 2; i < this.data.length(); i++) {
                    utf8String += this.data.charAt(i);
                }
                int outputDecimal = Integer.parseInt(utf8String, 16);
                this.data = Character.toString((char) outputDecimal);
                PrintStringSummary();
            } else if (this.outputType.equals("string")) {
                String utf8String = "";
                for (int i = 2; i < this.data.length(); i++) {
                    utf8String += this.data.charAt(i);
                }
                int outputDecimal = Integer.parseInt(utf8String, 16);
                this.data = Character.toString((char) outputDecimal);
                this.dataInt = this.data.codePointAt(0);
                System.out.print("String: ");
                System.out.println(Character.toChars(this.dataInt));
                EncodingHelperChar stringCharacter = new EncodingHelperChar(this.dataInt);
                System.out.println("Name: " + stringCharacter.getCharacterName());
            } else if (this.outputType.equals("utf8")) {
                ArrayList<String> utf8List = new ArrayList<String>();
                String utf8String = "";
                for (int i = 2; i < this.data.length(); i++) {
                    utf8String += this.data.charAt(i);
                }
                int outputDecimal = Integer.parseInt(utf8String, 16);
                this.data = Character.toString((char) outputDecimal);
                this.dataInt = this.data.codePointAt(0);
                EncodingHelperChar stringCharacter = new EncodingHelperChar(this.dataInt);
                utf8List.add(stringCharacter.toUTF8StringWithoutQuotes());
                System.out.print("UTF8: ");
                for (int k = 0; k < utf8List.size(); k++ ){
                    System.out.print(utf8List.get(k));
                }
                System.out.println();
            }
        }
    }

    public void NumberArgs(String[] args) {
        SortTypes(args);
        if (args.length == 1) {
            if (this.data.length() == 1) {
                PrintSingleCharacterSummary();
            } else {
                PrintStringSummary();
            }
        } else if (args.length == 2) {
            PrintWithInputOutput();
        } else if (args.length == 3) {
            PrintWithInputOutput();
        }
    }

    public void SortTypes(String[] args) {
        for(String arg : args) {
            if (arg.equals("--inputtype=codepoint")) {
                this.inputType = "codepoint";
            } else if (arg.equals("--inputtype=string")) {
                this.inputType = "string";
            } else if (arg.equals("--inputtype=utf8")) {
                this.inputType = "utf8";
            } else if (arg.equals("--outputtype=summary")) {
                this.outputType = "summary";
            } else if (arg.equals("--outputtype=string")) {
                this.outputType = "string";
            } else if (arg.equals("--outputtype=utf8")) {
                this.outputType = "utf8";
            } else if (arg.equals("--outputtype=codepoint")) {
                this.outputType = "codepoint";
            } else {
                this.data = arg;
            }
        }
    }

    public static void main(String[] args) {
        if (args.length == 0 || args.length > 3) {
            System.err.println("Usage: java EncodingHelper [--inputtype=type] [--outputtype=type] [data]");
            System.exit(1);
        }
        EncodingHelper encodingHelper = new EncodingHelper();
        encodingHelper.NumberArgs(args);
    }
}